package br.ucsal.bes20201.ed.atividade1.poo;

import java.util.Scanner;

public class Questao2 {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		obterNCalcularExibirE();
	}

	private static void obterNCalcularExibirE() {
		Integer n;
		Double e;

		n = obterNumero();
		e = calcularE(n);
		exibirE(n, e);
	}

	private static Integer obterNumero() {
		Integer n;
		System.out.println("Informe um n�mero maior ou igual a zero:");
		n = scanner.nextInt();
		return n;
	}

	private static Double calcularE(Integer n) {
		Double e = 0d;
		for (int i = 0; i <= n; i++) {
			e += 1d / calcularFatorial(i);
		}
		return e;
	}

	private static Long calcularFatorial(int n) {
		Long fat = 1L;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

	private static void exibirE(Integer n, Double e) {
		System.out.println("E(" + n + ")=" + e);
	}

}
