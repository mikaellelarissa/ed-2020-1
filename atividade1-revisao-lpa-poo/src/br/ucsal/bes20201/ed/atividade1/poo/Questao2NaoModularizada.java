package br.ucsal.bes20201.ed.atividade1.poo;

import java.util.Scanner;

public class Questao2NaoModularizada {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		Integer n;
		Double e;

		System.out.println("Informe um n�mero maior ou igual a zero:");
		n = scanner.nextInt();

		e = 0d;
		for (int i = 0; i <= n; i++) {

			// Calcular o fatorial de i, ou seja, fat(i)
			Long fat = 1L;
			for (int j = 1; j <= i; j++) {
				fat *= j;
			}

			e += 1d / fat;
		}

		System.out.println("E=" + e);
	}

}
