package br.ucsal.bes20201.ed.aula02;

public class Exemplo3 {

	private static final int QTD = 2;

	public static void main(String[] args) {

		Aluno[] alunos = new Aluno[QTD];

		alunos[0] = new Aluno();
		alunos[0].matricula = 123;
		alunos[0].nome = "Claudio Neiva";
		alunos[0].email = "claudio.neiva@ucsal.br";

		alunos[1] = new Aluno();
		alunos[1].matricula = 101;
		alunos[1].nome = "Maria da Silva";
		alunos[1].email = "maria.silva@ucsal.br";

		Aluno aux;
		for (int i = 0; i < QTD - 1; i++) {
			for (int j = i + 1; j < QTD; j++) {
				if (alunos[i].matricula > alunos[j].matricula) {
					aux = alunos[i];
					alunos[i] = alunos[j];
					alunos[j] = aux;
				}
			}
		}

		exibirAlunos(alunos);
	}

	private static void exibirAlunos(Aluno[] alunos) {
		for (int i = 0; i < QTD; i++) {
			System.out.println("Matrícula=" + alunos[i].matricula);
			System.out.println("Nome=" + alunos[i].nome);
			System.out.println("Email=" + alunos[i].email);
		}
	}

}
