package br.ucsal.bes20201.ed.aula02;

import java.util.Scanner;

public class Exemplo1 {

	private static final int QTD = 10;

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		obterSomarExibirArrays();
	}

	private static void obterSomarExibirArrays() {
		Integer[] vet1;
		Integer[] vet2;
		Integer[] vetSoma;

		vet1 = obterNumeros("Informe os n�meros para vet1=");
		vet2 = obterNumeros("Informe os n�meros para vet2=");
		vetSoma = somar(vet1, vet2);
		exibirSoma(vetSoma);
	}

	private static Integer[] obterNumeros(String mensagem) {
		Integer[] vet = new Integer[QTD];
		System.out.println(mensagem);
		for (int i = 0; i < vet.length; i++) {
			vet[i] = scanner.nextInt();
		}
		return vet;
	}

	private static Integer[] somar(Integer[] vet1, Integer[] vet2) {
		Integer[] vet = new Integer[QTD];
		for (int i = 0; i < vet1.length; i++) {
			vet[i] = vet1[i] + vet2[i];
		}
		return vet;
	}

	private static void exibirSoma(Integer[] vet) {
		System.out.println("A soma dos vetores �: ");
		for (int i = 0; i < vet.length; i++) {
			System.out.println(vet[i]);
		}
	}

}
