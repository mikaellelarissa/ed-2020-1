package br.ucsal.bes20201.ed.aula10;

public class ListaArray<T> implements Lista<T>{

	private static final int QTD_MAX = 100;
	
	private Object[] objetos = new Object[QTD_MAX];
	private T[] objetosTipado = (T[]) new Object[QTD_MAX];
	
	private int pos = 0;
	
	@Override
	public void add(T element) {
		objetos[pos] = element;
		pos++;
	}

	@Override
	public T get(int index) {
		return (T) objetos[pos];
	}

	public T getTipado(int index) {
		return objetosTipado[pos];
	}

	@Override
	public T remove(int index) {
		T element = (T) objetos[pos];
		// Remover o elemento aqui..
		return element;
	}

	public T removeTipado(int index) {
		T element = objetosTipado[pos];
		// Remover o elemento aqui..
		return element;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

}
