package br.ucsal.bes20201.ed.aula10;

public interface Lista<T> {
	
	void add(T element);
	
	T get(int index);
	
	T remove(int index);
	
	int size();
	
	boolean isEmpty();
	
	void clear();

}
