package br.ucsal.bes20201.ed.aula06reposicao;

public class ListaAlunoEncadeada implements ListaAluno {

	private No inicio;

	private int qtd = 0;

	@Override
	public void add(Aluno element) {
		No novoNo = new No(element);
		if (inicio == null) {
			inicio = novoNo;
		} else {
			No aux = inicio;
			// Esse laço faz com que aux aponte para o último elemento da lista.
			while (aux.prox != null) {
				aux = aux.prox;
			}
			// Adiciona o novoNo à lista, no final da mesma.
			aux.prox = novoNo;
		}
		qtd++;
	}

	@Override
	public Aluno get(int index) {
		No aux = inicio;
		for (int i = 0; i < index; i++) {
			aux = aux.prox;
		}
		return aux.element;
	}

	@Override
	public Aluno remove(int index) {
		No noAnterior = null;
		No aux = inicio;
		for (int i = 0; i < index; i++) {
			noAnterior = aux;
			aux = aux.prox;
		}
		// aqui aux aponta para o elemento que está no índice "index"
		// aqui noAnterior aponta para o no anterior ao que deve ser removido

		Aluno aluno = aux.element;

		if (noAnterior == null) {
			inicio = aux.prox;
		} else {
			noAnterior.prox = aux.prox;
		}

		qtd--;
		return aluno;
	}

	@Override
	public int size() {
		return qtd;
	}

	@Override
	public boolean isEmpty() {
		return inicio == null;
		// return qtd == 0;
	}

	@Override
	public void clear() {
		inicio = null;
		qtd = 0;
	}

	private class No {
		Aluno element;
		No prox;

		public No(Aluno element) {
			this.element = element;
		}
	}

}
