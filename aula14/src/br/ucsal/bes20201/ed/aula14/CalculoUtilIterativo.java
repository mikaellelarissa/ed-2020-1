package br.ucsal.bes20201.ed.aula14;

public class CalculoUtilIterativo implements CalculoUtil {

	@Override
	public Long calcularFatorial(Integer n) {
		Long fatorial = 1L;
		for (int i = 1; i <= n; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

	// 5! = 5 * 4 * 3 * 2 * 1

	// 5! = 1 * 2 * 3 * 4 * 5

	// 1 * 2 = 2
	// 2 * 3 = 6
	// 6 * 4 = 24
	// 24 * 5 = 120

}
