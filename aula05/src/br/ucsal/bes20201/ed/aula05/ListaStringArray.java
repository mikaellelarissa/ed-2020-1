package br.ucsal.bes20201.ed.aula05;

public class ListaStringArray implements ListaString {

	private static final int QTD_MAX = 1000;

	private static final int INICIO = 0;

	private String[] vet = new String[QTD_MAX];

	private int fim = INICIO;

	// FIXME Tratar o tamanho m�ximo da lista (n�o deve ser poss�vel adicionar
	// quando chegar em MAX_QTD).
	public void add(String element) {
		vet[fim] = element;
		fim++;
	}

	// FIXME Tratar o index em rela��o aos elementos dispon�veis no vetor.
	public String get(int index) {
		return vet[index];
	}

	// FIXME Tratar o index em rela��o aos elementos dispon�veis no vetor.
	public String remove(int index) {
		String item = get(index);
		for (int i = index; i < fim - 1; i++) {
			vet[i] = vet[i + 1];
		}
		fim--;
		vet[fim] = null;
		// vet[fim - 1] = null;
		// fim --;
		return item;
	}

	public int size() {
		return fim;
	}

	public boolean isEmpty() {
		return fim == INICIO;
//		if (fim == INICIO) {
//			return true;
//		} else {
//			return false;
//		}
	}

	// FIXME Se a lista for de objetos, essa implementa��o vai gerar problemas.
	public void clear() {
		fim = INICIO;
	}

}
