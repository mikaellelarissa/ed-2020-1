package br.ucsal.bes20201.ed.aula05;

public class Exemplo1 {
	
	public static void main(String[] args) {
		executarExemplo1();
	}

	private static void executarExemplo1() {
		ListaString nomes = new ListaStringArray();
		nomes.add("antonio");
		nomes.add("claudio");
		nomes.add("maria");
		nomes.add("ana");
		System.out.println("Primeiro nome na lista="+nomes.get(0));
		System.out.println("Segudno nome na lista (antes da remo��o)="+nomes.get(1));
		System.out.println("Tamanho da lista (antes de remo��o)="+nomes.size());
		nomes.remove(1);
		System.out.println("Segundo nome na lista (ap�s a remo��o)="+nomes.get(1));
		System.out.println("Tamanho da lista (ap�s a remo��o)="+nomes.size());
		System.out.println("A lista est� vazia?"+nomes.isEmpty());
		System.out.println("Limpei a lista...");
		nomes.clear();
		System.out.println("Tamanho da lista="+nomes.size());
		System.out.println("A lista est� vazia?"+nomes.isEmpty());
	}
}
