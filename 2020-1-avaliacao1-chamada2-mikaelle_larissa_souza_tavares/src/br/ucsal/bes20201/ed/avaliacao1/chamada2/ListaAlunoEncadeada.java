package br.ucsal.bes20201.ed.avaliacao1.chamada2;

public class ListaAlunoEncadeada implements ListaAluno {

	private No inicio;

	private No novoNo;

	private int qtd = 0;

	public void  ListaEncadeada() {
		Aluno element = null;
		novoNo = new No(element);
	}

	public void add(Aluno element) {
		if (inicio == null) {
			inicio = novoNo;
		} else {
			No aux = inicio;
			while (aux.prox != null) {
				aux = aux.prox;
			}
			aux.prox = novoNo;
		}
		qtd++;
	}

	public Aluno get(int index) {
		No aux = inicio;
		for (int i = 0; i < index; i++) {
			aux = aux.prox;
		}
		return aux.element;
	}

	public Aluno remove(int index) {
		No antigoNo = novoNo;
		No aux = novoNo;
		for (int i = 0; i < index; i++) {
			antigoNo = aux;
			aux = aux.prox;
		}
		Aluno element = aux.element;
		antigoNo.prox = aux.prox;
		qtd--;
		return null;
	}

	public void removeAll(ListaAluno alunosRemover) {
		alunosRemover = null;
	}

	public int size() {
		return qtd;
	}

	public int sizeNaoTemBoaPerformance() {
		int qtd = 0;
		No aux = inicio;
		while (aux != null) {
			qtd++;
			aux = aux.prox;
		}
		return qtd;
	}

	public boolean isEmpty() {
		return inicio == null;
	}

	public void clear() {
		inicio = null;
		qtd = 0;
	}

	private class No {
		Aluno element;
		No prox;

		public No(Aluno element) {
			this.element = element;
		}
	}

}

