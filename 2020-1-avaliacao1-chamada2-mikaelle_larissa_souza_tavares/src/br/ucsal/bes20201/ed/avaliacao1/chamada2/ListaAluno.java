package br.ucsal.bes20201.ed.avaliacao1.chamada2;

public interface ListaAluno {
	
	void add(Aluno element);
	
	Aluno get(int index);
	
	Aluno remove(int index);
	
	int size();
	
	boolean isEmpty();
	
	void clear();
	
	void removeAll(ListaAluno alunosRemover);

}
