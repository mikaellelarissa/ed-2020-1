package br.ucsal.bes20201.ed.atividade03.persistence;

import br.ucsal.bes20201.ed.atividade03.domain.Contato;
import br.ucsal.bes20201.ed.atividade03.tad.ArrayContatoList;
import br.ucsal.bes20201.ed.atividade03.tad.ContatoList;

public class ContatoDAO {

	private static ContatoList contatos = new ArrayContatoList();

	public static void incluir(Contato contato) {
		contatos.add(contato);
	}

	public static Contato pesquisar(String nome) {
		int index;
		Contato contato = null;

		index = contatos.indexOf(nome);
		if (index >= 0) {
			contato = contatos.get(index);
		}

		return contato;
	}

	// FIXME Substituir o retorno da lista completa de contatos por um clone dessa
	// lista.
	public static ContatoList obterTodos() {
		return contatos;
	}

}
