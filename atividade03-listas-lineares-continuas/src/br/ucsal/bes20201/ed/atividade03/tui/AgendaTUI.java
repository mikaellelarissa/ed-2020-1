package br.ucsal.bes20201.ed.atividade03.tui;

import java.util.Scanner;

public class AgendaTUI {

	private static final int INCLUIR_CONTATO = 1;
	private static final int PESQUISAR_CONTATO = 2;
	private static final int LISTAR_CONTATOS = 3;
	private static final int SAIR = 9;

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		executarAgenda();
	}

	private static void executarAgenda() {
		int opcao;
		do {
			exibirOpcoes();
			opcao = obterOpcao();
			executarOpcao(opcao);
		} while (opcao != SAIR);
	}

	private static void executarOpcao(int opcao) {
		switch (opcao) {
		case INCLUIR_CONTATO:
			ContatoTUI.incluir();
			break;
		case PESQUISAR_CONTATO:
			ContatoTUI.pesquisar();
			break;
		case LISTAR_CONTATOS:
			ContatoTUI.listar();
			break;
		case SAIR:
			System.out.println("Bye...");
			break;
		default:
			System.out.println("Opção incorreta.");
			break;
		}
	}

	private static int obterOpcao() {
		int opcao;
		System.out.println("Informe a opção desejada:");
		opcao = sc.nextInt();
		sc.nextLine();
		return opcao;
	}

	private static void exibirOpcoes() {
		System.out.println("\n");
		System.out.println(INCLUIR_CONTATO + " - Incluir contato");
		System.out.println(PESQUISAR_CONTATO + " - Pesquisar contato");
		System.out.println(LISTAR_CONTATOS + " - Listar contato");
		System.out.println(SAIR + " - Sair");
	}

}
