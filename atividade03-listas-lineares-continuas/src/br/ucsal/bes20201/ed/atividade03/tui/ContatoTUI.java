package br.ucsal.bes20201.ed.atividade03.tui;

import java.util.Scanner;

import br.ucsal.bes20201.ed.atividade03.business.ContatoBO;
import br.ucsal.bes20201.ed.atividade03.domain.Contato;
import br.ucsal.bes20201.ed.atividade03.tad.ContatoList;

public class ContatoTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void incluir() {
		System.out.println("************* INCLUSÃO DE CONTATOS *************");
		String nome;
		String telefone;
		String endereco;

		System.out.println("Informe:");
		System.out.println("Nome:");
		nome = sc.nextLine();
		System.out.println("Telefone:");
		telefone = sc.nextLine();
		System.out.println("Endereço:");
		endereco = sc.nextLine();

		Contato contato = new Contato(nome, telefone, endereco);

		ContatoBO.incluir(contato);
	}

	public static void pesquisar() {
		System.out.println("************* PESQUISA DE CONTATOS *************");
		String nome;

		System.out.println("Informe o nome para pesquisa:");
		nome = sc.nextLine();

		Contato contato = ContatoBO.pesquisar(nome);

		if (contato == null) {
			System.out.println("Contato não encontrado.");
		} else {
			System.out.println(contato);
		}

	}

	public static void listar() {
		System.out.println("************* LISTA DE CONTATOS *************");

		ContatoList contatos = ContatoBO.obterTodos();
		
		for (int i = 0; i < contatos.size(); i++) {
			System.out.println(contatos.get(i));
		}
	}

}
