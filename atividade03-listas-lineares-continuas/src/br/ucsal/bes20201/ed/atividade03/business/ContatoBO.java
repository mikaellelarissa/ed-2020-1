package br.ucsal.bes20201.ed.atividade03.business;

import br.ucsal.bes20201.ed.atividade03.domain.Contato;
import br.ucsal.bes20201.ed.atividade03.persistence.ContatoDAO;
import br.ucsal.bes20201.ed.atividade03.tad.ContatoList;

public class ContatoBO {

	public static void incluir(Contato contato) {
		// Validar o contato!
		// Qualquer regra de validação como:
		// 1. Tamanho do nome;
		// 2. Formatação de telefone;
		// 3. Tamanho do endereço;
		// 4. Obrigatoriedade de dados;
		// 5. Se o nome já existir na agenda, atualizar os dados no lugar de incluir um
		// novo contato;

		ContatoDAO.incluir(contato);
	}

	public static Contato pesquisar(String nome) {
		// Validar o nome a ser pesquisado;
		// Preencher dados nulos;
		// Ocultar dados sigilosos;

		return ContatoDAO.pesquisar(nome);
	}

	public static ContatoList obterTodos() {
		// Excluir elementos que não deveriam estar na lista;
		// Preencher dados nulos de cada elemento da lista;
		// Ocultar dados sigilosos de cada elemento da lista;
		
		return ContatoDAO.obterTodos();
	}

}
