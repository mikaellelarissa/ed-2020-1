package br.ucsal.bes20201.ed.aula12;

public class VerificacaoBalanceamentoTestSemJunit2 {

	public static void main(String[] args) {
		String expressao1 = "( 3 + 4 ) * [ 3 + 5 * ( 4 + 2 ) ] * [ 3 * ( 2 + 1 ) + 9 ]";
		System.out.println(
				expressao1 + " excepted true isBalanceada=" + VerificacaoBalanceamento.isExpressaoBalanceada(expressao1));
		String expressao2 = "( 3 + 4 ) * [ 3 + 5 * ( 4 + 2 ] ) * [ 3 * ( 2 + 1 ) + 9 ]";
		System.out.println(
				expressao2 + " excepted false isBalanceada=" + VerificacaoBalanceamento.isExpressaoBalanceada(expressao2));
		String expressao3 = "( 3 + 4 ) * ( 3 + 5 * [ 4 + 2 ) ] * [ 3 * ( 2 + 1 ) + 9 ]";
		System.out.println(expressao3 + " excepted false isBalanceada="
				+ VerificacaoBalanceamento.isExpressaoBalanceada(expressao3));
		String expressao4 = "( 3 + 4 ]";
		System.out.println(expressao4 + " excepted false isBalanceada="
				+ VerificacaoBalanceamento.isExpressaoBalanceada(expressao4));
		String expressao5 = "( 3 + 4 ) * [ 3 + 5 * ( 4 + 2 )";
		System.out.println(expressao5 + " excepted false isBalanceada="
				+ VerificacaoBalanceamento.isExpressaoBalanceada(expressao5));
	}

}
