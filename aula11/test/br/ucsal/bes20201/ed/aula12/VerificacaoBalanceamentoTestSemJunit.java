package br.ucsal.bes20201.ed.aula12;

public class VerificacaoBalanceamentoTestSemJunit {

	public static void main(String[] args) {
		testarBalanceada();
		testarNaoBalanceada1();
		testarNaoBalanceada2();
		testarNaoBalanceada3();
		testarNaoBalanceada4();
	}

	public static void testarBalanceada() {
		String expressao = "( 3 + 4 ) * [ 3 + 5 * ( 4 + 2 ) ] * [ 3 * ( 2 + 1 ) + 9 ]";
		System.out.println(
				expressao + " excepted true isBalanceada=" + VerificacaoBalanceamento.isExpressaoBalanceada(expressao));
	}

	public static void testarNaoBalanceada1() {
		String expressao = "( 3 + 4 ) * [ 3 + 5 * ( 4 + 2 ] ) * [ 3 * ( 2 + 1 ) + 9 ]";
		System.out.println(
				expressao + " excepted false isBalanceada=" + VerificacaoBalanceamento.isExpressaoBalanceada(expressao));
	}

	public static void testarNaoBalanceada2() {
		String expressao = "( 3 + 4 ) * ( 3 + 5 * [ 4 + 2 ) ] * [ 3 * ( 2 + 1 ) + 9 ]";
		System.out.println(expressao + " excepted false isBalanceada="
				+ VerificacaoBalanceamento.isExpressaoBalanceada(expressao));
	}

	public static void testarNaoBalanceada3() {
		String expressao = "( 3 + 4 ]";
		System.out.println(expressao + " excepted false isBalanceada="
				+ VerificacaoBalanceamento.isExpressaoBalanceada(expressao));
	}

	public static void testarNaoBalanceada4() {
		String expressao = "( 3 + 4 ) * [ 3 + 5 * ( 4 + 2 )";
		System.out.println(expressao + " excepted false isBalanceada="
				+ VerificacaoBalanceamento.isExpressaoBalanceada(expressao));
	}

}
