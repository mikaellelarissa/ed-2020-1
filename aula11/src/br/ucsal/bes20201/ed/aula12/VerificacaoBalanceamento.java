package br.ucsal.bes20201.ed.aula12;

import java.util.Scanner;

public class VerificacaoBalanceamento {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		executarVerificacaoBalanceamento();
	}

	private static void executarVerificacaoBalanceamento() {
		String expressao = obterExpressao();
		Boolean isExpressaoBalanceada = isExpressaoBalanceada(expressao);
		exibirSituacaoBalanceamentoExpressao(expressao, isExpressaoBalanceada);
	}

	private static String obterExpressao() {
		System.out.println("Informe a expressão:");
		return scanner.nextLine();
	}

	// Este método foi definido como público para viabilizar os testes automatizados
	// sem o uso de frameworks.
	public static Boolean isExpressaoBalanceada(String expressao) {
		Stack<Character> delimitadores = new ArrayStack<>();
		for (int i = 0; i < expressao.length(); i++) {
			Character character = expressao.charAt(i);

			if (character.equals('(') || character.equals('[')) {

				delimitadores.push(character);

			} else if (character.equals(')') || character.equals(']')) {

				Character delimitadorStack = delimitadores.pop();

				if ((character.equals(')') && !delimitadorStack.equals('('))
						|| (character.equals(']') && !delimitadorStack.equals('['))) {
					return false;
				}
			}
		}
		if (!delimitadores.isEmpty()) {
			return false;
		}
		return true;
	}

	private static void exibirSituacaoBalanceamentoExpressao(String expressao, Boolean isExpressaoBalanceada) {
		String descricaoSituacao;
		if (isExpressaoBalanceada) {
			descricaoSituacao = "está balanceada.";
		} else {
			descricaoSituacao = "NÃO está balanceada.";
		}
		System.out.println(expressao + " " + descricaoSituacao);
	}

}
