package br.ucsal.bes20201.ed.aula12;

/**
 * Implementação da pilha utilizando lista ligada como estrutura para
 * armazenamento dos dados da pilha.
 * 
 * @author claudioneiva
 *
 */
public class LinkedStack<T> implements Stack<T> {

	private No top;

	private int size = 0;

	@Override
	public void push(T element) {
		// TODO Auto-generated method stub

	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T top() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	private class No {
		private T element;
		private No prox;

		private No(T element) {
			this.element = element;
		}
	}

}
