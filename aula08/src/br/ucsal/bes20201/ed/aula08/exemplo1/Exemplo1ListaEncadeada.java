package br.ucsal.bes20201.ed.aula08.exemplo1;

import java.util.Scanner;

public class Exemplo1ListaEncadeada {

	private static Scanner sc = new Scanner(System.in);

	/*
	 * Obs: é DESNECESSÁRIO inicializar atributos com null, esta atribuição está
	 * aqui apenas para chamar a atenção de que o início da lista deve apontar para
	 * null;
	 */
	private static Aluno inicio = null;

	public static void main(String[] args) {
		obterAlunos();
		exibirAlunos();
	}

	private static void exibirAlunos() {
		System.out.println("Alunos cadastrados:");
		Aluno aux = inicio;
		while (aux != null) {
			System.out.println(aux);
			aux = aux.prox;
		}
	}

	private static void obterAlunos() {
		String resposta;
		Integer matricula;
		String nome;
		Aluno aluno;
		do {
			System.out.println("Informe os dados dos alunos:");
			System.out.println("Matrícula:");
			matricula = sc.nextInt();
			sc.nextLine();
			System.out.println("Nome:");
			nome = sc.nextLine();

			aluno = new Aluno(matricula, nome);

			if (inicio == null) {
				inicio = aluno;
			} else {
				Aluno aux = inicio;

				// Esse laço faz com que aux aponte para o último elemento da lista.
				while (aux.prox != null) {
					aux = aux.prox;
				}

				// Esse comando faz com que o elemento aluno seja encadeado à lista, no final da
				// mesma.
				aux.prox = aluno;
			}

			System.out.println("Deseja informar mais um aluno? (S/N):");
			resposta = sc.nextLine();
		} while (resposta.equalsIgnoreCase("S"));

	}

}
