package br.ucsal.bes20201.ed.atividade02;

import java.util.Scanner;

/*
Leia os dados de 10 estudantes (matricula, nome, email);
Ordene os estudantes por ordem crescente de matricula;
Exibia os dados dos estudantes, ap�s a ordena��o.
*/
public class Questao03 {

	private static final int QTD = 10;
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		executarQuestao03();
	}

	private static void executarQuestao03() {
		Estudante[] estudantes = obterEstudantes();
		ordenar(estudantes);
		exibir(estudantes);
	}

	private static void exibir(Estudante[] estudantes) {
		for (int i = 0; i < QTD; i++) {
			System.out.println(estudantes[i].matricula + " - " + estudantes[i].nome + " - " + estudantes[i].email);
		}
	}

	private static void ordenar(Estudante[] estudantes) {
		Estudante aux;
		for (int i = 0; i < QTD - 1; i++) {
			for (int j = i + 1; j < QTD; j++) {
				if (estudantes[i].matricula > estudantes[j].matricula) {
					aux = estudantes[i];
					estudantes[i] = estudantes[j];
					estudantes[j] = aux;
				}
			}
		}
	}

	private static Estudante[] obterEstudantes() {
		Estudante[] estudantes = new Estudante[QTD];

		// Entrada de dados
		for (int i = 0; i < QTD; i++) {
			System.out.println("Estudante #" + i);

			estudantes[i] = new Estudante();

			System.out.println("Matr�cula:");
			estudantes[i].matricula = sc.nextInt();
			sc.nextLine();

			System.out.println("Nome:");
			estudantes[i].nome = sc.nextLine();

			System.out.println("Email:");
			estudantes[i].email = sc.nextLine();
		}
		return estudantes;
	}

}
