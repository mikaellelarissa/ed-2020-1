package br.ucsal.bes20201.ed.atividade02;

public class Questao01 {

	private static final int QTD = 3;

	public static void main(String[] args) {
		executarQuestao01();
	}

	private static void executarQuestao01() {
		Vetor vet1 = new Vetor(QTD);
		Vetor vet2 = new Vetor(QTD);
		Vetor vet3;
		
		vet1.obter("Informe os dados do vetor1:");
		vet2.obter("Informe os dados do vetor2:");
		
		vet3 = vet1.somar(vet2);
		
		vet3.exibir("Soma dos vetores=");
	}
	
}
