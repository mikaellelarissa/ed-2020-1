package br.ucsal.bes20201.ed.aula13;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class QueueTest {

	/*
	 * FIXME Refatorar esse teste para separar as verificações de element, isEmpty e
	 * size.
	 */
	@Test
	public void testarAddRemoveElementEmpty1() {
		Queue<String> queue1 = new ArrayQueue<>();

		queue1.add("antonio");
		queue1.add("claudio");
		queue1.add("pedreira");
		queue1.add("neiva");

		Assertions.assertEquals(4, queue1.size());

		Assertions.assertFalse(queue1.isEmpty());

		Assertions.assertEquals("antonio", queue1.element());
		Assertions.assertEquals("antonio", queue1.remove());

		Assertions.assertEquals(3, queue1.size());

		Assertions.assertEquals("claudio", queue1.element());
		Assertions.assertEquals("claudio", queue1.element());
		Assertions.assertEquals("claudio", queue1.element());
		Assertions.assertEquals("claudio", queue1.remove());

		Assertions.assertEquals("pedreira", queue1.remove());

		Assertions.assertEquals("neiva", queue1.remove());

		Assertions.assertEquals(0, queue1.size());

		Assertions.assertTrue(queue1.isEmpty());

		queue1.add("ana");
		queue1.add("clara");

		Assertions.assertEquals(2, queue1.size());

		Assertions.assertEquals("ana", queue1.remove());
		Assertions.assertEquals("clara", queue1.remove());

	}

	@Test
	public void testarAdicionarElementosAlemCapacidadeFila() {
		Queue<String> queue1 = new ArrayQueue<>();

		queue1.add("antonio");
		queue1.add("claudio");
		queue1.add("pedreira");
		queue1.add("neiva");
		queue1.add("joaquim");

		Assertions.assertThrows(RuntimeException.class, () -> {
			queue1.add("ana");
		});

		Assertions.assertEquals("antonio", queue1.remove());

	}

}
