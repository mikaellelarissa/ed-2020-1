package br.ucsal.bes20201.ed.arvore;

public class Node {

	// Atributos para implementação do conceito de árvore binária.

	Object element;

	Node left;

	Node right;

	/*
	 * Atributos para apresentação na interface Java Swing e NÃO fazem parte da
	 * solução de árvore binária.
	 */

	Boolean selected = false;

	int sequence;

	// Construtor.
	
	public Node(Object element) {
		this.element = element;
	}
}
