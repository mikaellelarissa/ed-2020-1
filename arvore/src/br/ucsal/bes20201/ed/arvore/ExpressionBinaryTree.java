package br.ucsal.bes20201.ed.arvore;

import java.util.StringTokenizer;

/*
 * https://pt.wikipedia.org/wiki/Nota%C3%A7%C3%A3o_polonesa_inversa
 */
public class ExpressionBinaryTree extends BinaryTree {

	private static final long serialVersionUID = 1L;

	private static final Boolean DEBUG = false;

	public ExpressionBinaryTree() {

	}

	public ExpressionBinaryTree(String expression) {
		assembleTree(expression);
	}

	public Double solve() {
		showVisit = false;
		return Double.parseDouble(postOrderVisit().toString());
	}

	public void assembleExpression() {
		showVisit = true;
		preOrderVisit();
		System.out.println();
	}

	public void assembleTree(String expression) {
		StringTokenizer stringTokenizer = new StringTokenizer(expression);
		Stack<Node> stack = new ArrayStack<>();
		while (stringTokenizer.hasMoreTokens()) {
			String token = stringTokenizer.nextToken();
			Node node = new Node(token);
			if ("+-*/".indexOf(token) >= 0) {
				stack.push(node);
			} else {
				resolve(stack, node);
			}
		}
		root = stack.pop();
	}

	private void resolve(Stack<Node> stack, Node node) {
		if (!stack.isEmpty()) {
			Node stackNode = stack.pop();
			if (stackNode.left == null) {
				stackNode.left = node;
				stack.push(stackNode);
			} else {
				stackNode.right = node;
				resolve(stack, stackNode);
			}
			if (stack.isEmpty()) {
				stack.push(stackNode);
			}
		}
	}

	@Override
	protected Object visit(Node node) {
		return super.visit(node);
	}

	protected Object postOrderVisit(Node node) {
		if (node != null) {
			String leftElement = postOrderVisit(node.left).toString();
			String rightElement = postOrderVisit(node.right).toString();
			String nodeElement = visit(node).toString();
			String resultExpression = leftElement + " " + nodeElement + " " + rightElement;
			if (!leftElement.isEmpty()) {
				String result = executeOperation(leftElement, rightElement, nodeElement).toString();
				if (DEBUG) {
					System.out.println(resultExpression + " = " + result);
				}
				node.element += " = " + result;
				return result;
			}
			return resultExpression;
		}
		return "";
	}

	private Double executeOperation(String leftElement, String rightElement, String nodeElement) {
		Double leftValue = Double.parseDouble(leftElement);
		Double rightValue = Double.parseDouble(rightElement);
		switch (nodeElement) {
		case "+":
			return leftValue + rightValue;
		case "-":
			return leftValue - rightValue;
		case "*":
			return leftValue * rightValue;
		case "/":
			return leftValue / rightValue;
		}
		return 0d;
	}

	@Override
	protected String createContent(Node node) {
		return node.element.toString();
	}

	@Override
	protected void showVisit(Node node) {
		System.out.print(node.element.toString() + " ");
	}

}
