package br.ucsal.bes20201.ed.arvore;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class BinaryTree extends JPanel {

	private static final long serialVersionUID = 1L;

	private static final int POS_X = 600;
	private static final int POS_Y = 50;
	private static final int INC_Y = 30;
	private static final int[] INC_X = { 300, 150, 90, 30, 10, 10, 10 };
	private static final int WIDTH_BOX = 50;
	private static final int ARCH_EDGE = 10;
	private static final int TEXT_ADJUST_Y = 4;
	
	private static final long PAUSE = 500;

	private static int sequence = -1;

	public static final boolean DEBUG = false;

	protected boolean showVisit = true;
	
	protected Node root;

	public Node addRoot(Object element) {
		Node newNode = new Node(element);
		root = newNode;
		return newNode;
	}

	public Node addLeft(Node node, Object element) {
		Node newNode = new Node(element);
		node.left = newNode;
		return newNode;
	}

	public Node addRight(Node node, Object element) {
		Node newNode = new Node(element);
		node.right = newNode;
		return newNode;
	}

	public void preOrderVisit() {
		sequence = 0;
		clearSelect(root);
		preOrderVisit(root);
	}

	public void inOrderVisit() {
		sequence = 0;
		clearSelect(root);
		inOrderVisit(root);
	}

	public void reverseInOrderVisit() {
		sequence = 0;
		clearSelect(root);
		reverseInOrderVisit(root);
	}

	public Object postOrderVisit() {
		sequence = 0;
		clearSelect(root);
		return postOrderVisit(root);
	}

	public int height() {
		return height(root) - 1;
	}

	public int nodes() {
		return nodes(root);
	}

	protected void preOrderVisit(Node node) {
		if (node != null) {
			visit(node);
			preOrderVisit(node.left);
			preOrderVisit(node.right);
		}
	}

	protected void inOrderVisit(Node node) {
		if (node != null) {
			inOrderVisit(node.left);
			visit(node);
			inOrderVisit(node.right);
		}
	}

	protected void reverseInOrderVisit(Node node) {
		if (node != null) {
			reverseInOrderVisit(node.right);
			visit(node);
			reverseInOrderVisit(node.left);
		}
	}

	protected Object postOrderVisit(Node node) {
		if (node != null) {
			postOrderVisit(node.left);
			postOrderVisit(node.right);
			return visit(node);
		}
		return null;
	}

	private int height(Node node) {
		if (node == null) {
			return 0;
		}
		int leftHeight = height(node.left);
		int rightHeight = height(node.right);
		return Math.max(leftHeight, rightHeight) + 1;
	}

	private int nodes(Node node) {
		if (node != null) {
			return 1 + nodes(node.left) + nodes(node.right);
		}
		return 0;
	}

	protected Object visit(Node node) {
		pause();
		sequence++;
		node.sequence = sequence;
		node.selected = true;
		showVisit(node);
		repaint();
		return node.element;
	}

	protected void showVisit(Node node) {
		if (showVisit) {
			System.out.println(node.element);
		}
	}

	private void clearSelect(Node node) {
		if (node != null) {
			node.sequence = 0;
			node.selected = false;
			clearSelect(node.left);
			clearSelect(node.right);
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		drawNode(root, POS_X, POS_Y, POS_X, POS_Y, 0, g);
	}

	private void drawNode(Node node, int posX, int posY, int posXFather, int posYFather, int level, Graphics g) {
		if (node != null) {
			if (node.selected) {
				g.setColor(Color.GREEN);
			} else {
				g.setColor(Color.CYAN);
			}
			g.fillRoundRect(posX - WIDTH_BOX / 2, posY - INC_Y / 2, 100, INC_Y, ARCH_EDGE, ARCH_EDGE);
			g.setColor(Color.BLACK);
			if (level != 0) {
				g.drawLine(posXFather + WIDTH_BOX / 2, posYFather + INC_Y / 2, posX, posY - INC_Y / 2);
			}
			g.drawRoundRect(posX - WIDTH_BOX / 2, posY - INC_Y / 2, 100, INC_Y, ARCH_EDGE, ARCH_EDGE);
			g.drawString(createContent(node), posX, posY + TEXT_ADJUST_Y);
			drawNode(node.left, posX - INC_X[level], posY + 2 * INC_Y, posX, posY, level + 1, g);
			drawNode(node.right, posX + INC_X[level], posY + 2 * INC_Y, posX, posY, level + 1, g);
		}
	}

	protected String createContent(Node node) {
		String content = node.element.toString();
		if (node.sequence > 0) {
			content += "(" + node.sequence + ")";
		}
		return content;
	}

	private void pause() {
		try {
			Thread.sleep(PAUSE);
		} catch (InterruptedException e) {
		}
	}

}
