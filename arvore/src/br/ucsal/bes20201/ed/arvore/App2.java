package br.ucsal.bes20201.ed.arvore;

import java.awt.Color;

import javax.swing.JFrame;

public class App2 {

	private static final int WINDOW_WIDTH = 1250;

	private static final int WINDOW_HEIGHT = 500;

	private ExpressionBinaryTree expressionBinaryTree;

	public static void main(String[] args) throws InterruptedException {
		new App2().exec();
	}

	private void exec() throws InterruptedException {
		expressionBinaryTree = new ExpressionBinaryTree("/ * + 3 9 - 7 1 - 13 4");
		createJFrame();
		expressionBinaryTree.assembleExpression();
		expressionBinaryTree.solve();
	}

	private JFrame createJFrame() {
		JFrame jFrame = new JFrame("Árvore Binária");
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		jFrame.setBackground(Color.WHITE);
		jFrame.add(expressionBinaryTree);
		jFrame.setVisible(true);
		return jFrame;
	}

}
