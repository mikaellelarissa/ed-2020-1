package br.ucsal.bes20201.ed.arvore;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingWorker;

public class App implements ActionListener {

	private static final int WINDOW_WIDTH = 1250;

	private static final int WINDOW_HEIGHT = 500;

	private static final int BUTTON_WIDTH = WINDOW_WIDTH / 4 - 10;

	private static final int BUTTON_HEIGHT = 40;

	private BinaryTree binaryTree;

	private JButton preOrderButton;
	private JButton inOrderButton;
	private JButton reverseInOrderButton;
	private JButton postOrderButton;

	public static void main(String[] args) throws InterruptedException {
		new App().exec();
	}

	private void exec() throws InterruptedException {
		binaryTree = createBinaryTree();
		createJFrame();
		System.out.println("nós=" + binaryTree.nodes());
		System.out.println("altura=" + binaryTree.height());
	}

	private BinaryTree createBinaryTree() {
		BinaryTree binaryTree = new BinaryTree();
		Node node1 = binaryTree.addRoot("Antonio");
		Node node2 = binaryTree.addLeft(node1, "Claudio");
		Node node3 = binaryTree.addRight(node1, "Pedreira");
		Node node4 = binaryTree.addLeft(node2, "João");
		Node node5 = binaryTree.addRight(node2, "Maria");
		Node node6 = binaryTree.addLeft(node3, "Joaquim");
		Node node7 = binaryTree.addRight(node3, "Tereza");
		binaryTree.addLeft(node4, "Ana");
		binaryTree.addRight(node4, "Clara");
		binaryTree.addLeft(node5, "Pedro");
		binaryTree.addRight(node5, "Jessica");
		binaryTree.addLeft(node6, "Camila");
		binaryTree.addRight(node6, "Jasmine");
		binaryTree.addLeft(node7, "Lucas");
		binaryTree.addRight(node7, "Mateus");
		return binaryTree;
	}

	private JFrame createJFrame() {
		JFrame jFrame = new JFrame("Árvore Binária");
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);

		JPanel buttonPanel = createButtonPanel(jFrame);

		JSplitPane splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane.setDividerLocation(BUTTON_HEIGHT + 10);
		
		splitPane.setTopComponent(buttonPanel);
		splitPane.setBottomComponent(binaryTree);

		jFrame.add(splitPane);

		jFrame.setVisible(true);

		return jFrame;
	}

	private JPanel createButtonPanel(JFrame jFrame) {
		JPanel buttonPanel = new JPanel();
		preOrderButton = createButton("PRE-ORDER", jFrame);
		inOrderButton = createButton("IN-ORDER", jFrame);
		reverseInOrderButton = createButton("REVERSE-IN-ORDER", jFrame);
		postOrderButton = createButton("POST-ORDER", jFrame);
		buttonPanel.add(preOrderButton);
		buttonPanel.add(inOrderButton);
		buttonPanel.add(reverseInOrderButton);
		buttonPanel.add(postOrderButton);
		return buttonPanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(preOrderButton)) {
			new ButtonWorker(() -> binaryTree.preOrderVisit()).execute();
		} else if (e.getSource().equals(inOrderButton)) {
			new ButtonWorker(() -> binaryTree.inOrderVisit()).execute();
		} else if (e.getSource().equals(reverseInOrderButton)) {
			new ButtonWorker(() -> binaryTree.reverseInOrderVisit()).execute();
		} else if (e.getSource().equals(postOrderButton)) {
			new ButtonWorker(() -> binaryTree.postOrderVisit()).execute();
		}
	}

	private JButton createButton(String text, JFrame jFrame) {
		JButton jButton = new JButton(text);
		jButton.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		jButton.addActionListener(this);
		return jButton;
	}

	private class ButtonWorker extends SwingWorker<Integer, Void> {

		private Runnable runnable;

		public ButtonWorker(Runnable runnable) {
			this.runnable = runnable;
		}

		@Override
		protected Integer doInBackground() throws Exception {
			runnable.run();
			return null;
		}
	}

}
