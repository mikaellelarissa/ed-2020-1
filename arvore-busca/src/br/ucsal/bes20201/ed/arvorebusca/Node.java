package br.ucsal.bes20201.ed.arvorebusca;

public class Node<T extends Comparable<T>> {

	// Atributos para implementação do conceito de árvore binária.

	T element;

	Node<T> left;

	Node<T> right;

	int height = 0;

	/*
	 * Atributos para apresentação na interface Java Swing e NÃO fazem parte da
	 * solução de árvore binária.
	 */

	Boolean selected = false;

	int sequence;

	// Construtor.

	public Node(Node<T> left, T element, Node<T> right) {
		this.element = element;
		this.left = left;
		this.right = right;
	}

	public int getBalanceFactor() {
		int leftHeigh = (left == null) ? -1 : left.height;
		int rightHeigh = (right == null) ? -1 : right.height;
		return rightHeigh - leftHeigh;
	}

	@Override
	public String toString() {
		return element.toString();
	}
}
