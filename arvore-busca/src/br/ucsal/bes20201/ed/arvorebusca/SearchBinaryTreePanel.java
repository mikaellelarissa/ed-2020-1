package br.ucsal.bes20201.ed.arvorebusca;

import java.awt.Color;
import java.awt.Graphics;

public class SearchBinaryTreePanel<T extends Comparable<T>> extends SearchBinaryTree<T> {

	private static final long serialVersionUID = 1L;

	private static final int POS_X = 600;
	private static final int POS_Y = 50;
	private static final int INC_Y = 30;
	private static final int[] INC_X = { 300, 150, 90, 30, 10, 10, 10 };
	private static final int WIDTH_BOX = 110;
	private static final int ARCH_EDGE = 10;
	private static final int TEXT_ADJUST_X = -1 * WIDTH_BOX / 2 + 5;
	private static final int TEXT_ADJUST_Y = 4;

	private static final long PAUSE = 500;

	private static int sequence = -1;

	public static final boolean DEBUG = false;

	protected boolean showVisit = true;

	protected void visit(Node<T> node) {
		pause();
		sequence++;
		node.selected = true;
		showVisit(node);
		repaint();
	}

	protected void showVisit(Node<T> node) {
		if (showVisit) {
			System.out.println(node.element);
		}
	}

	protected void clearSelect(Node<T> node) {
		if (node != null) {
			sequence = 0;
			node.sequence = 0;
			node.selected = false;
			clearSelect(node.left);
			clearSelect(node.right);
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		drawNode(root, POS_X, POS_Y, POS_X, POS_Y, 0, g);
	}

	private void drawNode(Node<T> node, int posX, int posY, int posXFather, int posYFather, int level, Graphics g) {
		if (node != null) {
			if (node.selected) {
				g.setColor(Color.GREEN);
			} else {
				g.setColor(Color.CYAN);
			}
			g.fillRoundRect(posX - WIDTH_BOX / 2, posY - INC_Y / 2, WIDTH_BOX, INC_Y, ARCH_EDGE, ARCH_EDGE);
			g.setColor(Color.BLACK);
			if (level != 0) {
				g.drawLine(posXFather, posYFather + INC_Y / 2, posX, posY - INC_Y / 2);
			}
			g.drawRoundRect(posX - WIDTH_BOX / 2, posY - INC_Y / 2, WIDTH_BOX, INC_Y, ARCH_EDGE, ARCH_EDGE);
			g.drawString(createContent(node), posX + TEXT_ADJUST_X, posY + TEXT_ADJUST_Y);
			drawNode(node.left, posX - INC_X[level], posY + 2 * INC_Y, posX, posY, level + 1, g);
			drawNode(node.right, posX + INC_X[level], posY + 2 * INC_Y, posX, posY, level + 1, g);
		}
	}

	protected String createContent(Node<T> node) {
		String content = node.element.toString() + "[" + node.height + "|" + node.getBalanceFactor() + "]";
		if (node.sequence > 0) {
			content += "(" + node.sequence + ")";
		}
		return content;
	}

	private void pause() {
		try {
			Thread.sleep(PAUSE);
		} catch (InterruptedException e) {
		}
	}

}
