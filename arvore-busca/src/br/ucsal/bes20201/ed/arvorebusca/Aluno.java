package br.ucsal.bes20201.ed.arvorebusca;

public class Aluno implements Comparable<Aluno> {

	private Integer matricula;

	private String nome;

	private String email;

	public Aluno(Integer matricula) {
		super();
		this.matricula = matricula;
	}

	public Aluno(Integer matricula, String nome, String email) {
		this(matricula);
		this.nome = nome;
		this.email = email;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return matricula + "-" + nome;
	}

	@Override
	public int compareTo(Aluno outroAluno) {
		return matricula.compareTo(outroAluno.matricula);
	}

}
