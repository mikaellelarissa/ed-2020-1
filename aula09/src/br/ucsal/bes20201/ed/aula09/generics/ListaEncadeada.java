package br.ucsal.bes20201.ed.aula09.generics;

public class ListaEncadeada<T> implements Lista<T> {

	private No inicio;

	private int qtd = 0;

	@Override
	public void add(T element) {
		No novoNo = new No(element);
		if (inicio == null) {
			inicio = novoNo;
		} else {
			No aux = inicio;
			// Esse laço faz com que aux aponte para o último elemento da lista.
			while (aux.prox != null) {
				aux = aux.prox;
			}
			// Adiciona o novoNo à lista, no final da mesma.
			aux.prox = novoNo;
		}
		qtd++;
	}

	@Override
	public T get(int index) {
		No aux = inicio;
		for (int i = 0; i < index; i++) {
			aux = aux.prox;
		}
		return aux.element;
	}

	@Override
	public T remove(int index) {
		//TODO Implementar a remoção!!!!
		qtd--;
		return null;
	}

	@Override
	public int size() {
		return qtd;
	}

	public int sizeNaoTemBoaPerformance() {
		int qtd = 0;
		No aux = inicio;
		while (aux != null) {
			qtd++;
			aux = aux.prox;
		}
		return qtd;
	}

	@Override
	public boolean isEmpty() {
		return inicio == null;
		//return qtd == 0;
	}

	@Override
	public void clear() {
		inicio = null;
		qtd = 0;
	}

	private class No {
		private T element;
		private No prox;

		private No(T element) {
			this.element = element;
		}
	}

}
