package br.ucsal.bes20201.ed.hanoi;

/**
 * 
 * Especifica um pilha, com o seguinte comportamento:
 * 
 * 1. Insere no topo e remove do topo;
 * 
 * 2. O primeiro a ser colocado na pilha será o último a sair da mesma. FILO
 * (First In Last Out).
 * 
 * @author claudioneiva
 *
 * @param <T> tipo de dado que pode ser manipulado pela pilha.
 */
public interface Stack<T> {

	/**
	 * Incluir um elemento na pilha (sempre no topo).
	 * 
	 * @param element elemento a ser inserido.
	 */
	void push(T element);

	/**
	 * Retirar o elemento do topo da pilha.
	 * 
	 * @return o elemento do topo da pilha.
	 */
	T pop();

	/**
	 * Observar o elemento do topo da pilha, sem remover.
	 * 
	 * @return o elemento do topo da pilha.
	 */
	T top();

	/**
	 * Retornar se a pilha está vazia.
	 * 
	 * @return true se a pilha estiver vazia e false se pilha tiver algum elemento.
	 */
	boolean isEmpty();

	/**
	 * Retornar a quantidade de elementos na pilha.
	 * 
	 * @return quantidade de elementos presentes na pilha.
	 */
	int size();
	
	Stack<T> clonar();
	
	void clear();
}
