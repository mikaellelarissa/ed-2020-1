package br.ucsal.bes20201.ed.hanoi;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Painel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private static final int TORRE1_X = 110;
	private static final int TORRE2_X = 410;
	private static final int TORRE3_X = 710;

	private static final int TORRE_Y = 100;
	private static final int PINO_TORRE_ALTURA = 300;
	private static final int PINO_TORRE_LARGURA = 10;

	private Stack<Disco> torre1;
	private Stack<Disco> torre2;
	private Stack<Disco> torre3;

	public Painel(Stack<Disco> torre1, Stack<Disco> torre2, Stack<Disco> torre3) {
		super();
		this.torre1 = torre1;
		this.torre2 = torre2;
		this.torre3 = torre3;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		desenharTorre(torre1, TORRE1_X, g);
		desenharTorre(torre2, TORRE2_X, g);
		desenharTorre(torre3, TORRE3_X, g);
	}

	private void desenharTorre(Stack<Disco> torre, int posXTorre, Graphics g) {
		g.setColor(Color.GRAY);
		g.fillRect(posXTorre - PINO_TORRE_LARGURA/2, TORRE_Y, PINO_TORRE_LARGURA, PINO_TORRE_ALTURA);
	
		Stack<Disco> clone = torre.clonar();
		int posYDisco = (PINO_TORRE_ALTURA + TORRE_Y)- clone.size() * Disco.ALTURA;
		while (!clone.isEmpty()) {
			Disco disco = clone.pop();
			g.setColor(disco.getCor());
			g.fillRoundRect(posXTorre - disco.getDiametro() / 2, posYDisco, disco.getDiametro(), Disco.ALTURA, Disco.ARCO_BORDA, Disco.ARCO_BORDA);
			posYDisco += Disco.ALTURA;
		}
	}

}