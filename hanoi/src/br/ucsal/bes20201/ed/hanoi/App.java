package br.ucsal.bes20201.ed.hanoi;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Resolvendo manualmente: https://www.somatematica.com.br/jogos/hanoi/
 * 
 * Solução recursiva:
 * https://panda.ime.usp.br/pythonds/static/pythonds_pt/04-Recursao/09-hanoi.html
 *
 * Mova a torre de altura−1 para o pino intermediário, usando o pino destino
 * como intermediário.
 * 
 * Mova o disco restante para o pino destino.
 *
 * Mova a torre de altura−1 do pino intermediário para o pino destino usando o
 * pino origem como intermediário
 *
 * Solução iterativa:
 * https://www.devmedia.com.br/torres-de-hanoi-solucao-iterativa-em-java/23761
 *
 */
public class App {

	private static final int PAUSA_ENTRE_MOVIMENTOS_DISCOS = 1000;

	private static JFrame janela;

	public static void main(String[] args) throws InterruptedException {
		executarTorresHanoi();
	}

	private static void executarTorresHanoi() throws InterruptedException {

		janela = criarJanela();

		Stack<Disco> torre1 = new ArrayStack<>();
		Stack<Disco> torre2 = new ArrayStack<>();
		Stack<Disco> torre3 = new ArrayStack<>();
		Painel painel = new Painel(torre1, torre2, torre3);
		janela.add(painel);

		inicializarTorres(torre1, torre2, torre3);
		JOptionPane.showMessageDialog(janela, "Solução recursiva.", janela.getTitle(), JOptionPane.WARNING_MESSAGE);
		moverDiscosRecursivo(painel, torre1.size(), torre1, torre3, torre2);

		inicializarTorres(torre1, torre2, torre3);
		JOptionPane.showMessageDialog(janela, "Solução iterativa.", janela.getTitle(), JOptionPane.WARNING_MESSAGE);
		moverDiscosIterativo(painel, torre1.size(), torre1, torre3, torre2);

	}

	private static void inicializarTorres(Stack<Disco> torre1, Stack<Disco> torre2, Stack<Disco> torre3) {
		torre1.clear();
		torre2.clear();
		torre3.clear();
		// torre1.push(new Disco(180, Color.CYAN));
		// torre1.push(new Disco(150, Color.YELLOW));
		torre1.push(new Disco(120, Color.PINK));
		torre1.push(new Disco(90, Color.GREEN));
		torre1.push(new Disco(60, Color.RED));
		torre1.push(new Disco(30, Color.BLUE));
	}

	private static JFrame criarJanela() {
		JFrame janela = new JFrame("Torres de HANOI");
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janela.setSize(860, 460);
		janela.setVisible(true);
		return janela;
	}

	private static void moverDiscosRecursivo(Painel painel, int qtd, Stack<Disco> torreOrigem,
			Stack<Disco> torreDestino, Stack<Disco> torreTrabalho) throws InterruptedException {

		if (qtd > 0) {
			// Mova a torre de altura − 1 para o pino intermediário, usando o pino de
			// destino como intermediário
			moverDiscosRecursivo(painel, qtd - 1, torreOrigem, torreTrabalho, torreDestino);

			// Mova o disco restante para o pino destino.
			torreDestino.push(torreOrigem.pop());

			// Pausar e depois fazer um refresh do painel (redesenhar o painel).
			Thread.sleep(PAUSA_ENTRE_MOVIMENTOS_DISCOS);
			painel.repaint();

			// Mova a torre de altura − 1 do pino intermediário para o pino destino usando o
			// pino origem como intermediário
			moverDiscosRecursivo(painel, qtd - 1, torreTrabalho, torreDestino, torreOrigem);
		}
	}

	private static void moverDiscosIterativo(Painel painel, int qtd, Stack<Disco> torreOrigem,
			Stack<Disco> torreDestino, Stack<Disco> torreTrabalho) throws InterruptedException {
		Stack<Comando> comandos = new ArrayStack<>();
		Comando comandoAtual = new Comando(qtd, torreOrigem, torreDestino, torreTrabalho);
		comandos.push(comandoAtual);
		while (!comandos.isEmpty()) {
			if (qtd > 1) {
				qtd--;
				torreOrigem = comandoAtual.getTorreOrigem();
				torreDestino = comandoAtual.getTorreDestino();
				torreTrabalho = comandoAtual.getTorreTrabalho();
				comandoAtual = new Comando(qtd, torreOrigem, torreTrabalho, torreDestino);
				comandos.push(comandoAtual);
			} else {
				comandoAtual = comandos.pop();
				qtd = comandoAtual.getQtd();
				comandoAtual.getTorreDestino().push(comandoAtual.getTorreOrigem().pop());
				painel.repaint();
				Thread.sleep(PAUSA_ENTRE_MOVIMENTOS_DISCOS);
				if (qtd > 1) {
					qtd--;
					comandoAtual = new Comando(qtd, comandoAtual.getTorreTrabalho(), comandoAtual.getTorreDestino(),
							comandoAtual.getTorreOrigem());
					comandos.push(comandoAtual);
				}
			}
		}
	}

}
