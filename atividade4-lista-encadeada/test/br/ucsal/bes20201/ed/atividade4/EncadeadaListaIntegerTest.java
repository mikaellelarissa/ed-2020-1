package br.ucsal.bes20201.ed.atividade4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EncadeadaListaIntegerTest {

	@Test
	public void testarGet1Elemento() {
		EncadeadaListaInteger lista = new EncadeadaListaInteger();
		lista.add(45);
		Assertions.assertEquals(45, lista.get(0));
	}

	@Test
	public void testarGet3Elementos() {
		EncadeadaListaInteger lista = new EncadeadaListaInteger();
		lista.add(45);
		lista.add(30);
		lista.add(67);
		Assertions.assertEquals(45, lista.get(0));
		Assertions.assertEquals(30, lista.get(1));
		Assertions.assertEquals(67, lista.get(2));
		Assertions.assertNull(lista.get(3));
	}

	@Test
	public void testarAddAll() {
		EncadeadaListaInteger lista1 = new EncadeadaListaInteger();
		lista1.add(45);
		lista1.add(30);
		lista1.add(67);

		EncadeadaListaInteger lista2 = new EncadeadaListaInteger();
		lista2.add(87);
		lista2.add(12);
		lista2.add(75);
		lista2.add(43);

		lista1.addAll(lista2);

		lista2.add(77);

		Assertions.assertEquals(45, lista1.get(0));
		Assertions.assertEquals(30, lista1.get(1));
		Assertions.assertEquals(67, lista1.get(2));
		Assertions.assertEquals(87, lista1.get(3));
		Assertions.assertEquals(12, lista1.get(4));
		Assertions.assertEquals(75, lista1.get(5));
		Assertions.assertEquals(43, lista1.get(6));
		Assertions.assertNull(lista1.get(7));

		Assertions.assertEquals(87, lista2.get(0));
		Assertions.assertEquals(12, lista2.get(1));
		Assertions.assertEquals(75, lista2.get(2));
		Assertions.assertEquals(43, lista2.get(3));
		Assertions.assertEquals(77, lista2.get(4));
		Assertions.assertNull(lista2.get(5));
	}
}
