package br.ucsal.bes20201.ed.atividade4;

public interface ListaInteger {

	/**
	 * Adiciona um elemento no final da lista atual.
	 * 
	 * @param element elemento a ser adicionado.
	 */
	void add(Integer element);

	/**
	 * Retorna o elemento da lista, na posição especificada.
	 * 
	 * @param index posição do elemento a ser retornado
	 * @return o elemento da posição especificada
	 */
	Integer get(int index);

	/**
	 * Adiciona todas os elementos de uma lista no final da lista atual .
	 *
	 * @param list lista que contem os elementos a serem adicionados à lista atual.
	 */
	void addAll(ListaInteger list);

	/**
	 * Retorna a quantidade de elementos da lista. Esse método facilitou a
	 * implementação do addAll, mas não era essencial. Contruimos uma implementação
	 * de addAll com size e outra sem o size.
	 * 
	 * @return quantidade de elementos na lista.
	 */
	int size();

}