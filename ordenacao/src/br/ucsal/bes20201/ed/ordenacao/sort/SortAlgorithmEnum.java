package br.ucsal.bes20201.ed.ordenacao.sort;

public enum SortAlgorithmEnum {

	SELECTION_SORT(1, "Selection sort"),

	BUBBLE_SORT(2, "Bubble sort"),

	INSERTION_SORT(3, "Insertion sort"),

	MERGE_SORT(4, "Merge sort");

	private Integer cod;
	private String description;

	private SortAlgorithmEnum(Integer cod, String description) {
		this.cod = cod;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public String getFullDescription() {
		return cod + " - " + description;
	}

	public static SortAlgorithmEnum valueOfCod(Integer cod) {
		for (SortAlgorithmEnum sortAlgorithm : values()) {
			if (sortAlgorithm.cod.equals(cod)) {
				return sortAlgorithm;
			}
		}
		throw new IllegalArgumentException(
				"Cod " + cod + " not found in " + SortAlgorithmEnum.class.getCanonicalName());
	}

}
