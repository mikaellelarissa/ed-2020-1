package br.ucsal.bes20201.ed.ordenacao.search;

public enum SearchAlgorithmEnum {

	SEQUENCIAL_SEARCH(1, "Sequencial seach"),

	BINARY_SEARCH_ITERATIVE(2, "Binary search iterative "),

	BINARY_SEARCH_RECURSIVE(3, "Binary search recursive ");

	private Integer cod;
	private String description;

	private SearchAlgorithmEnum(Integer cod, String description) {
		this.cod = cod;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public String getFullDescription() {
		return cod + " - " + description;
	}

	public static SearchAlgorithmEnum valueOfCod(Integer cod) {
		for (SearchAlgorithmEnum sortAlgorithm : values()) {
			if (sortAlgorithm.cod.equals(cod)) {
				return sortAlgorithm;
			}
		}
		throw new IllegalArgumentException(
				"Cod " + cod + " not found in " + SearchAlgorithmEnum.class.getCanonicalName());
	}

}
