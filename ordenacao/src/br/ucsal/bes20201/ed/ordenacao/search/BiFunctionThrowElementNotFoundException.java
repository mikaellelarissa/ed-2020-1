package br.ucsal.bes20201.ed.ordenacao.search;

public interface BiFunctionThrowElementNotFoundException<T, U, R> {
    R apply(T t, U u) throws ElementNotFoundException;
}